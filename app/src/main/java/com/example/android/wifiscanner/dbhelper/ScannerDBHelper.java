package com.example.android.wifiscanner.dbhelper;


import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by nyotie on 10-Mar-16.
 */
public class ScannerDBHelper extends SQLiteOpenHelper{

    // If you change the database schema, you must increment the database version.
    private static final int DATABASE_VERSION = 1;

    public static final String DATABASE_NAME = "wifirec.db";
    public static final String WIFI_TABLE_NAME = "wifi_list";
    public static final String WIFI_COLUMN_ID = "_id";
    public static final String WIFI_COLUMN_SSID = "ssid";
    public static final String WIFI_COLUMN_RSSI = "rssi";
    public static final String WIFI_COLUMN_DATE = "recdate";

    public ScannerDBHelper(Context context){
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(String.format("create table %s (%s integer primary key, %s text, %s text, %s text);",
                        WIFI_TABLE_NAME, WIFI_COLUMN_ID, WIFI_COLUMN_SSID, WIFI_COLUMN_RSSI, WIFI_COLUMN_DATE)
        );
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + WIFI_TABLE_NAME + ";");
        onCreate(db);
    }

    public boolean insertNewRow (String ssid, String rssi) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(WIFI_COLUMN_SSID, ssid);
        contentValues.put(WIFI_COLUMN_RSSI, rssi);
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = new Date();
        contentValues.put(WIFI_COLUMN_DATE, dateFormat.format(date));

        db.insert(WIFI_TABLE_NAME, null, contentValues);
        return true;
    }

    public Cursor getData(int id){
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res =  db.rawQuery( "SELECT * FROM " + WIFI_TABLE_NAME + " where " + WIFI_COLUMN_ID + " = " + id + ";", null );
        return res;
    }

    public int numberOfRows(){
        SQLiteDatabase db = this.getReadableDatabase();
        int numRows = (int) DatabaseUtils.queryNumEntries(db, WIFI_TABLE_NAME);
        return numRows;
    }

    public boolean updateContact(Integer id, String ssid, String rssi, String recDate) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(WIFI_COLUMN_SSID, ssid);
        contentValues.put(WIFI_COLUMN_RSSI, rssi);
        contentValues.put(WIFI_COLUMN_DATE, recDate);
        db.update(WIFI_TABLE_NAME, contentValues, WIFI_COLUMN_ID + " = ? ", new String[]{Integer.toString(id)});
        return true;
    }

    public Integer deleteContact (Integer id) {
        SQLiteDatabase db = this.getWritableDatabase();
        return db.delete(WIFI_TABLE_NAME,
                WIFI_COLUMN_ID + " = ? ",
                new String[] { Integer.toString(id) });
    }
}
