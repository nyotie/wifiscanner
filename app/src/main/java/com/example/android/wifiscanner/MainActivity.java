package com.example.android.wifiscanner;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.android.wifiscanner.dbhelper.ScannerDBHelper;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.List;

//1. choose one of the wifi --> still cant automatically use connected wifi
//2. refresh the level value every 5s
//3. save the refreshed value if flag=true --> how to write-update the file

public class MainActivity extends AppCompatActivity {
    Boolean isRecording = false;
    static final int READ_BLOCK_SIZE = 100; // size of read buffer

    // tools
    EditText choosenOne;
    TextView mainText;

    // wifi thingy
    WifiManager mainWifi;
    WifiReceiver receiverWifi;
    List<ScanResult> wifiList;
    StringBuilder sb = new StringBuilder();

    // databases
    ScannerDBHelper mydb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mainText = (TextView) findViewById(R.id.mainText);
        choosenOne = (EditText) findViewById(R.id.theChoosenOne);

        // Initiate wifi service manager
        mainWifi = (WifiManager) getSystemService(Context.WIFI_SERVICE);
        // Check for wifi is disabled
        if (!mainWifi.isWifiEnabled())
        {
            // If wifi disabled then enable it
            Toast.makeText(getApplicationContext(), "wifi is disabled..making it enabled", Toast.LENGTH_LONG).show();
            mainWifi.setWifiEnabled(true);
        }

        // wifi scanned value broadcast receiver
        receiverWifi = new WifiReceiver();

        // Register broadcast receiver
        // Broadcast receiver will automatically call when number of wifi connections changed
        registerReceiver(receiverWifi, new IntentFilter(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION));

        // set-up and call the automaton
        mHandler = new Handler();

        // init db tools
        mydb = new ScannerDBHelper(this);

        // initialize the start scan
        mainWifi.startScan();
        mainText.setText("Starting Scan...");
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        menu.add(0, 0, 0, "Refresh");
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        mainWifi.startScan();
        mainText.setText("Starting Scan");
        Log.i("Menu item", "Force refresh");
        return super.onOptionsItemSelected(item);
    }

    protected void onPause() {
        unregisterReceiver(receiverWifi);
        super.onPause();
    }

    protected void onResume() {
        registerReceiver(receiverWifi, new IntentFilter(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION));
        super.onResume();
    }

    class WifiReceiver extends BroadcastReceiver {

        // This method call when number of wifi connections changed
        public void onReceive(Context c, Intent intent) {

            sb = new StringBuilder();
            wifiList = mainWifi.getScanResults(); // fill the variable with result from scan
            sb.append("\n        Number Of Wifi connections :" + wifiList.size() + "\n\n");

            for(int i = 0; i < wifiList.size(); i++){
                sb.append(new Integer(i+1).toString() + ". "); //number
                sb.append((wifiList.get(i)).SSID + " (fre:"); //ssid
                sb.append((wifiList.get(i)).frequency + ", dBm:"); //frequency value
                sb.append((wifiList.get(i)).level + ")"); //dBm value
                sb.append("\n\n");

                // write the data to textfile
                // bug : if the SSID is not detected will not write anything
                if (isRecording && wifiList.get(i).SSID.equals(choosenOne.getText().toString())){
                    Toast.makeText(getBaseContext(), "level:" + Integer.toString(wifiList.get(i).level), Toast.LENGTH_SHORT).show();
                    Log.i("Record msg", "SSID found");
                    WriteData(wifiList.get(i).SSID, Integer.toString(wifiList.get(i).level));
                }
            }
            mainText.setText(sb);
        }
    }

    // -------------automaton
    private int mInterval = 5000; // 5 seconds by default, can be changed later
    private Handler mHandler;

    Runnable mStatusChecker = new Runnable() {
        @Override
        public void run() {
            try {
                // updateStatus(); //this function can change value of mInterval.
                mainWifi.startScan();
                mainText.setText("Starting Scan");
                Log.i("Automaton", "Refresh every " + mInterval/1000 + "sec");
            } finally {
                // 100% guarantee that this always happens, even if
                // your update method throws an exception
                mHandler.postDelayed(mStatusChecker, mInterval);
            }
        }
    };

    // to be called by start button?
    void startRepeatingTask() {
        if(isRecording) {
            mStatusChecker.run();
        }
    }

    // to be called by stop
    void stopRepeatingTask() {
        mHandler.removeCallbacks(mStatusChecker);
    }
    // -------------end of automaton

    // write text to file
    private void WriteData(String ssidName, String levelValue) {
        // add-write text into file
        try {
            FileOutputStream fileOut = openFileOutput("wifiscanner_result.txt", MODE_PRIVATE); // MODE_PRIVATE or MODE_APPEND
            OutputStreamWriter outputWriter = new OutputStreamWriter(fileOut);
            outputWriter.append("SSID:" + ssidName + "; RSSI:" + levelValue + "\n"); // write or append
            outputWriter.close();
            Log.i("WriteFile", "Success");

            mydb.insertNewRow(ssidName, levelValue);
            Log.i("SQLite","Insert success");
        } catch (Exception e) {
            Log.e("WriteFile", "Error!");
            e.printStackTrace();
        }
    }

    // Read text from file
    public void ReadBtn(View v) {
        //reading text from file
        try {
            FileInputStream fileIn=openFileInput("wifiscanner_result.txt");
            InputStreamReader InputRead= new InputStreamReader(fileIn);

            char[] inputBuffer= new char[READ_BLOCK_SIZE];
            String s="";
            int charRead;

            while ((charRead=InputRead.read(inputBuffer))>0) {
                // char to string conversion
                String readstring=String.copyValueOf(inputBuffer,0,charRead);
                s +=readstring;
            }
            InputRead.close();

            // display value of textfile
            Toast.makeText(getBaseContext(), s, Toast.LENGTH_SHORT).show();
            Log.e("ReadFile", "Success");
        } catch (Exception e) {
            Log.e("ReadFile", "Error!");
            e.printStackTrace();
        }
    }

    // Delete file if exist
    public void DelBtn(View v){
        if(isRecording){ isRecording = false; }

        try {
            File dir = getFilesDir();
            File file = new File(dir, "wifiscanner_result.txt");
            boolean deleted = file.delete();

            Log.e("DelFile", "Success:" + deleted);
        } catch (Exception e){
            Log.e("DelFile", "Error!");
            e.printStackTrace();
        }
    }

    public void StartRecording(View v) {
        if (choosenOne.getText().toString().equals("")){
            isRecording = false;
            choosenOne.setEnabled(true);
            Toast.makeText(getBaseContext(), "Input SSID name", Toast.LENGTH_SHORT).show();
            Log.i("Record", "Failed to enable recording");
        } else {
            isRecording = true;
            choosenOne.setEnabled(false);
            Toast.makeText(getBaseContext(), "Recording enabled", Toast.LENGTH_SHORT).show();
            Log.i("Record", "Recording enabled");

            // call the automaton
            startRepeatingTask();
        }
    }
    public void StopRecording(View v){
        if(isRecording) {
            isRecording = false;
            choosenOne.setEnabled(true);
            Toast.makeText(getBaseContext(), "Recording disabled", Toast.LENGTH_SHORT).show();
            Log.i("Record", "Recording disabled");

            // call function to stop automaton
            stopRepeatingTask();
        }
    }
}
